# Building as HTML files

    ./configure
    make html

Your HTML output will then be in the `html/` directory, start with
`html/intro.html`

# Making a new page

1. Make a Markdown file that would be led to when compiled. For example,
if you look at the `intro.md` it refers to a `water/water.html`, you'll 
notice that if you look at `src/water/water.md` it'll contain the source 
for the `water/water.html`, when you write a new page, if it has options
A and B, it should link to `a/a.html` and `b/b.html`

2. Re-build the HTML file for your testing pleasure
    `./configure
     make html

3. Submit your page as a git pull request. Please only a page at a time.


