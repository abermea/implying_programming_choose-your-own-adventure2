# A late night

After a late night of shitposting and development and drinking you 
realize you forgot to drink water and are dehydrated and will likely 
suffer symptoms shortly. However, getting hydrated you must choose 
between one of a few options.

Your first option is to fill up your water pitcher and [drink the 
filtered water](water/water.html) While this is your best option for 
hydration, your filter may be going bad soon and also you gotta pay the water 
bill on that.

Your other option is to [drink watermelon 
juice](watermelon/watermelon.html) The best and worst option. Because it 
is spiked with sojo at approximately 8% ABV.

For the wildcard option, you can just [drink straight up 
soju](soju/soju.html) at 
17.5% 
ABV.
